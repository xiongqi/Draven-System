const ApiBase = `${window.location.protocol}//${window.location.hostname}:${sessionStorage.port || 9000}`;
export { ApiBase };
